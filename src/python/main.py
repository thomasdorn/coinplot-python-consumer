import pika
import json
from pymongo import MongoClient
from web3 import Web3
import multiprocessing
from bson import json_util
from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException
import logging
import subprocess
from decimal import Decimal

logging.basicConfig()
logging.getLogger("BitcoinRPC").setLevel(logging.INFO)

rpc_user = "8duamCpHBgKMZb5b"
rpc_password = "2GXvxhRmUtLnNkgP"

id = "coinplot_python_worker_0"


class fakefloat(float):
    def __init__(self, value):
        self._value = value

    def __repr__(self):
        return str(self._value)


def default_encode(o):
    if isinstance(o, Decimal):
        # Subclass float with custom repr?
        return fakefloat(o)
    raise TypeError(repr(o) + " is not JSON serializable")


class Worker(multiprocessing.Process):
    @staticmethod
    def ack_message(channel, delivery_tag):
        if channel.is_open:
            channel.basic_ack(delivery_tag)
        else:
            pass

    @staticmethod
    def rabbit_publish_to_user(ch, username, message, caller):
        ch.queue_declare(queue=username)
        if isinstance(message, list):
            msg = json.dumps(message)
        else:
            msg = json.dumps(message, indent=4, default=json_util.default)
        print(" [x] master_callback / " + caller + " => Sent to '" + username + "': %r" % msg)
        ch.basic_publish(exchange='', routing_key=username, body=msg)

    @staticmethod
    def get_tx_config(body):
        rpc = json.loads(body.decode())
        if 'txConfig' in rpc:
            return rpc['txConfig']

    @staticmethod
    def get_current_key(body):
        rpc = json.loads(body.decode())
        if 'currentKey' in rpc:
            return rpc['currentKey']

    @staticmethod
    def get_key_password(body):
        rpc = json.loads(body.decode())
        if 'keypassword' in rpc:
            return rpc['keypassword']

    @staticmethod
    def get_value0_for_run_jar(body):
        rpc = json.loads(body.decode())
        if 'digits' in rpc:
            return rpc['digits']

    def run(self) -> None:
        name = multiprocessing.current_process().name
        print(name, 'starting')
        eth_provider = Web3.IPCProvider('/home/workspace/coinploteth0/geth.ipc')
        w3 = Web3(eth_provider)
        client = MongoClient('mongodb://nodeapplication:AoIBAQDQrYa7w9GN3tMI@coinplot.info:27018/node-hapi-api',
                             ssl=True)

        def master_callback(ch, method, properties, body) -> None:
            print(" [x] process / master_callback => %r" % name + " received: %r" % body.decode())
            self.ack_message(ch, method.delivery_tag)
            rpc = json.loads(body.decode())
            username_queue = rpc['username']
            username = username_queue
            command = rpc['command']

            def eth_get_gas_price():
                gas_price = w3.eth.generateGasPrice()
                self.rabbit_publish_to_user(ch, username, gas_price, "eth_get_gas_price")

            def eth_send_transaction():
                tx_config = self.get_tx_config(body)
                db = client['node-hapi-api']
                keys = db.keys
                doc = keys.find({'username': username, 'ethereumKey': tx_config['from']})[0]
                try:
                    w3.personal.sendTransaction(tx_config, doc['keypassword'])
                    self.rabbit_publish_to_user(ch, username,
                                                "transaction sent successfully",
                                                "eth_send_transaction")
                except ValueError:
                    self.rabbit_publish_to_user(ch, username,
                                                "insufficient funds for gas * price + value",
                                                "eth_send_transaction")

            def eth_get_transaction_count():
                current_key = self.get_current_key(body)
                tx_count = w3.eth.getTransactionCount(current_key)
                self.rabbit_publish_to_user(ch, username, tx_count, "eth_get_transaction_count")

            def eth_check_balance():
                db = client['node-hapi-api']
                keys = db.keys
                docs = keys.find({'username': username_queue})
                balances = []
                for item in docs:
                    long_balance = w3.eth.getBalance(item['ethereumKey'])
                    human_balance = w3.fromWei(long_balance, 'ether')
                    balances.append(str(human_balance))
                self.rabbit_publish_to_user(ch, username, balances, "eth_check_balance")

            def eth_create_key():
                key_password = self.get_key_password(body)
                ethereum_key = w3.personal.newAccount(key_password)
                doc = {
                    "username": username,
                    "ethereumKey" : ethereum_key,
                    "keypassword": key_password
                }
                db = client['node-hapi-api']
                keys = db.keys
                keys.insert(doc)
                self.rabbit_publish_to_user(ch, username, doc, "eth_create_key")

            if command == 'ethCheckBalance':
                eth_check_balance()

            if command == 'ethSendTransaction':
                eth_send_transaction()

            if command == 'ethGetTransactionCount':
                eth_get_transaction_count()

            if command == 'ethGetGasPrice':
                eth_get_gas_price()

            if command == 'ethCreateKey':
                eth_create_key()

            if command == 'btcNodeStatus':
                rpc_connection = AuthServiceProxy("http://%s:%s@matto.dornlabs.com:8332" % (rpc_user, rpc_password))
                node_blockchain_info_str = json.dumps(rpc_connection.getblockchaininfo(), default=default_encode)
                doc = {
                    "info": json.loads(node_blockchain_info_str)
                }
                self.rabbit_publish_to_user(ch, username, doc, "btc_create_key")

            if command == 'btcCreateKey':
                try:
                    rpc_connection = AuthServiceProxy("http://%s:%s@matto.dornlabs.com:8332" % (rpc_user, rpc_password))
                    bitcoin_address = rpc_connection.getnewaddress()
                    doc = {
                        "username": username,
                        "bitcoinKey": bitcoin_address
                    }
                    db = client['node-hapi-api']
                    keys = db.keys
                    keys.insert(doc)
                    self.rabbit_publish_to_user(ch, username, doc, "btc_create_key")
                except BrokenPipeError:
                    self.rabbit_publish_to_user(ch, username, "error creating key", "btc_create_key")

            if command == 'btcCheckBalance':
                try:
                    rpc_connection = AuthServiceProxy("http://%s:%s@matto.dornlabs.com:8332" % (rpc_user, rpc_password))
                    balance = rpc_connection.getreceivedbyaddress("3GjkeW6P275Mm88tUC3v6Whus3Bpj5gFso")
                    balance_str = json.dumps(balance, default=default_encode)
                    doc = {
                        "balance" : json.loads(balance_str)
                    }
                    self.rabbit_publish_to_user(ch, username, doc, "btc_create_key")
                except Exception as e:
                    doc = {
                        "error": str(e)
                    }
                    self.rabbit_publish_to_user(ch, username, doc, "btc_check_balance")
            if command == 'runJarFile':
                try:
                    value0 = self.get_value0_for_run_jar(body)
                    hello = subprocess.check_output([
                        "java",
                        "-jar",
                        "./download_images-all-1.0.jar",
                        str(value0)
                    ]).decode('ascii')
                    doc = {
                        "response": hello
                    }
                    self.rabbit_publish_to_user(ch, username, doc, "run_jar_file")
                except Exception as e:
                    print(e)

        def filter_worker(ch, method, properties, body):
            if id in json.loads(body.decode())["workers"]:
                master_callback(ch, method, properties, body)
            else:
                # self.ack_message(ch, method.delivery_tag)
                # self.rabbit_publish_to_user(ch, "master", body, "filter_worker")
                print(" [x :: WARN] filter_worker / message bounce")

        credentials = pika.PlainCredentials("celeryuser", "celery")
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host='dornhost.com', virtual_host='celeryvhost', credentials=credentials))
        channel = connection.channel()
        channel.queue_declare(queue='master')
        channel.basic_consume('master', master_callback, auto_ack=False)

        try:
            while True:
                connection.process_data_events(time_limit=0.250)
        finally:
            connection.close()


if __name__ == '__main__':
    jobs = []
    for i in range(2):
        p = Worker()
        jobs.append(p)
        p.start()
    for j in jobs:
        j.join()
